import time
import pyvisa

"""#################SEARCH/CONNECT#################"""
# establish communication with scope
initial = time.time()
rm = pyvisa.ResourceManager()
#lecroy = rm.open_resource('TCPIP::130.87.242.122::INSTR')
lecroy = rm.open_resource('TCPIP::192.168.8.3::INSTR')
#lecroy = rm.open_resource('TCPIP::192.168.10.3::INSTR')
#lecroy = rm.open_resource('TCPIP::10.30.1.21::INSTR')
lecroy.timeout = 3000000
lecroy.encoding = 'latin_1'
lecroy.clear()

print("IDN? : ",lecroy.query("*IDN?"))
print("ALST? : ",lecroy.query("ALST?"))
lecroy.write("STOP")
lecroy.write("SEQ OFF")
lecroy.write("DISPLAY OFF")
lecroy.write("DISPLAY ON")
lecroy.close()
